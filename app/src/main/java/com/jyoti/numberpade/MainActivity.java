package com.jyoti.numberpade;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.jyoti.NumberPad;

public class MainActivity extends AppCompatActivity {
    TextView txt, remaining_digit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.hideStatusbar(this);
        setContentView(R.layout.activity_main);
        final NumberPad numpad = findViewById(R.id.num);
        txt = findViewById(R.id.txt);
        remaining_digit = findViewById(R.id.remaining_digit);
        remaining_digit.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/custom_font.ttf"));
        txt.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/custom_font_bold.ttf"));
        numpad.setOnTextChangeListner((String text, int digits_remaining) -> {
            if (text.length() == 0) {
                remaining_digit.setText("");
            } else {
                remaining_digit.setText("Remaining digits: " + digits_remaining + "/" + numpad.getTextLengthLimit());
            }
            txt.setText(text);
        });
    }
}